## Lizenz
## Aufgabe 1: Erklären Sie das Wort Lizenz. Was bedeutet das?
Eine Lizenz ist wie eine Erlaubnis, die jemand gibt, um bestimmte Rechte an einer Idee, einem Produkt oder einer Ressource an eine andere Person oder Firma weiterzugeben. Es ist, als ob jemand sagt: "Du darfst das benutzen, aber nur unter bestimmten Bedingungen." Diese Bedingungen sind in einem Lizenzvertrag festgelegt.
## Aufgabe 2: Warum kommt man auf die Idee, Lizenzen zu schaffen?
Lizenzen werden erstellt, um:

* Rechte zu schützen: Sie helfen dabei, geistiges Eigentum wie Software oder Kunstwerke vor unerlaubter Nutzung zu schützen.
 
* Kontrolle zu behalten: Lizenzen erlauben es den Inhabern, die Nutzung ihres Eigentums zu kontrollieren, indem sie Regeln festlegen.
* Geld zu verdienen: Durch den Verkauf von Lizenzen können Inhaber Einnahmen erzielen.
* Innovation zu fördern: Lizenzen fördern die Schöpfung neuer Ideen, da sie Schutz und Anreize bieten.
* Klarheit zu schaffen: Sie sorgen für klare rechtliche Vereinbarungen und vermeiden Missverständnisse.
## Aufgabe 3: Worauf gibt es alles Lizenzen ausser für Software?
#Lizenzen werden für viele Dinge außer Software verwendet, darunter:
* Kreatives geistiges Eigentum: Wie Bücher, Musik, Kunst und Filme.
* Patente: Für technische Erfindungen.
* Marken und Franchise: Für den Einsatz von Marken und Geschäftsmodellen.
* Gesundheitswesen und Bauwesen: Für die Ausübung von Berufen und Bauarbeiten.
* Gastronomie und Alkoholverkauf: Zum Betreiben von Restaurants und Bars.
* Medien und Sport: Für die Verwendung von Inhalten, Marken und Teamlogos.
## Aufgabe 4: Machen Sie eine Aufzählung von Lizenzmodellen.
* Proprietäre Lizenz
* Die Unternehmen behalten die Kontrolle über z.B den Code.
* Open-Source-Lizenz
* Der Anwender darf Änderungen am Quellcode der Software vornehmen
# Das sind untergeordnete Lizenzmodelle:
* Dauerlizenz
* Nach einem einmaligen Kauf, hat man für immer zugriff auf die Software.
* Abonnement-basierte Lizenz
* Nach einer Zahlung hat man für einen bestimmten Zeitraum zugriff auf die Software, jedoch muss man erneut zahlen, wenn die Zeit abgelaufen ist um den Zugriff zu verlängern.
* Floating-Lizenz
* Es können nur eine begrenzte Anzahl von Lizenzen gleichzeitig verwendet werden, dabei ist es egal wer.
* Node-Locked-Lizenz
* Eine Lizenz wird einem spezifischen Gerät zugewiesen.
* Public-Domain-Lizenz
* Es gibt keine Einschränkungen was mit der Software gemacht werden kann.
* Copyleft-Lizenz
* Wenn bei Open-Source-Software der Code verändert wird muss der Veränderer die veränderte Software unter den selben Lizenzbedingungen veröffentlichen.
* Windows CAL
* Man kann nur von einem Server aus auf Anwendungen zugreifen. Dafür braucht man eine Berechtigung sich mit dem Server zu verbinden
## Aufgabe 5: Was bedeutet "open source" und was kann man mit solcher Software machen?
"Open Source" bezieht sich auf Software, deren Quellcode für jeden frei einsehbar ist. Oft sind diese Software auch kostenlos, das muss aber nicht sein.
## Aufgabe 6: Was ist der Unterschied zwischen "copyright" und "copyleft"
"Copyright" schützt die Urheberrechte und ermöglicht es dem Urheber, die Verwendung seines Werks zu kontrollieren. "Copyleft" ist eine Philosophie, die das Urheberrecht nutzt, um sicherzustellen, dass ein Werk und seine Abwandlungen für alle offen und frei bleiben. Werke unter "Copyleft" müssen unter denselben Bedingungen geteilt werden.
## Aufgabe 7: Frage: 
* Welches Lizenz-Modell wird angewendet, 
* wenn man im App-Store eine zu bezahlende App heunterlädt und installiert?/im App-Store eine Gratis-App heunterlädt und installiert? 
*Haben Sie die Software bezahlt, als Sie ihr aktuelles Smartphone gekauft/bekommen haben? Wenn Sie es nicht wissen, ob Sie extra bezahlt haben, wie und wann bezahlen Sie?
im App-Store eine zu bezahlende App heunterlädt und installiert?
proprietären Lizenz, Dauerlizenz
im App-Store eine Gratis-App heunterlädt und installiert?
proprietären Lizenz, Dauerlizenz
