# m231-doku


## 29.08.2023

### Installation von Git

Ich habe Git installiert.
Das habe ich gemacht, indem ich "Git download" im Internet eingegeben habe.

### Profil von GitLab

Ich habe ein Profil auf GitLab erstellt und ein Projekt erstellt.

### Befehle in Powershell

Ich habe gelernt wie man über Windows Powershell Ordner und Dokumente in NotePad öffnen kann.
Mit "dir" kann man alle Ordner oder Dateien in einem Ordner sehen.
Mit "cd"+ ordner kann man den Ordner öffnen.
Mit der tab-taste kann man wenn es eindeutig ist die ersten Buchstaben eingeben und es automatisch auswählen lassen.
Mit "notepad"+ Dokument kann man ein Dokument mit Notepad öffnen.


## 05.09.2023

### Datenschutz und Datensicherheit unterscheiden
ich konnte herausfinden, dass man während sich beim Datenschutz ausschließlich mit personenbezogenen Daten befasst, besteht das Ziel bei der Datensicherheit darin, alle Arten von Informationen zu schützen, unabhängig davon, ob sie digital oder analog sind, von einer natürlichen Person stammen oder nicht. Wir haben uns auch mit dem Informationssicherheitdreieck befasst und zu dem gehört:Integrität, Vertraulichkeit und Verügbarkeit.

### Ablagekonzept 
Ich habe mein Eigenes Ablagekonzept erstellt und somit auch einsehen wie Die Dateien Transferiert werden.
Ich habe gesehen das Die Daten von Chatsapps und auch Emails nicht sicher sind, da es immer angeschaut kann werden von den Anbietern. Im Gegensatz sind Die Dokumente vom Teaching und Knowledgebase werden im Synology-Cloud (sichere Cloud) sowie auch im Onedrive von Microsoft Sihcer gesichert, sowie auch die repositories auf Github sind auch sicher. Es folgt noch das Abspeichern von Fotos was auch nicht mehr Vertrauenswürdig ist da google sowie deine Daten anschaut, aber auch bei Datenverlust nicht gut enden würde. 


## 12.09.2023

### [Datenschutz und Datensicherheit Leseauftrag](Datenschut&Datensicherheit-Leseauftrag.md) 


### Herausforderungen in der digitalen Welt
1. Widerstand gegen Veränderungen
2. Fehlende Vorstellung der digitalen Kanäle
3. Mangelnde Erfahrung im Bereich Innovation
4. Begrenztes Budget

### Hot-Backup vs Cold Backup
Wir hatten den Auftrag den Unterschied von Hot und Cold Backup zu erklären und wir fanden heraus das der Cold Backup verbunden ist mit USB-Stick und Festplatte, während Hot mehr mit der Icloud und Onedrive zutun hat. Beide haben ihre Vor und Nachteile. Hot-Backup wird durchgeführt während der Pc noch an ist, während Cold dann ausgeführt wird, wenn der Pc aus ist.

Umfrage- und Wettbewerbsformulare sowie Kredit- und Handelsauskunfteien sind weitere Quellen für persönliche Daten.
Datenschutz ist wichtig, um die Privatsphäre zu wahren und persönliche Daten vor Missbrauch zu schützen. Betroffene haben das Recht, ihre Daten einzusehen, korrigieren zu lassen oder löschen zu lassen.

### [Zusammenfassung](Zusammenfassung.md)

## 19.09.2023

### LB1
In der ersten Lektion haben wir eine Prüfung gehabt. Wir durften Open-book arbeiten. Dafür hatten wir gut viel Zeit.

### Schutz von Daten im Internet

Firewall und Virenschutz einschalten, starke Passwörter verwenden, keine verdächtigen Links und Emails öffnen, VPN verwenden
Vorischtig mit öffentlichen Wlans sein.

## 26.09.2023
[Backup-Konzept](backup.md)

[Passwortmanager](Passwort-Manager.md)

## 03.10.2023

[6A: Authentisierung, Authentifizierung, Autorisierung](6A.md)

[6B: Multifaktor Authentisierung](6b Multifaktor Authentisierung.md) 

## 24.10.2023
LB2-Prüfung haben wir schriftlich geschrieben.

[Impressum, Disclamer und AGBs](impressum-disclaimer-agb.md) 

[Problematik Datenlöschungen](Problematik-datenlöschungen.md)

## 31.10.2023

[Lizensenmodell](Lizensenmodell.md)

## 07.11.2023
Modulende & Abgabe der LB3.
