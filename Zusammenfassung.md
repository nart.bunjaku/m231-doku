## Zusammenfassung

Welche Herausforderungen bringt die Digitalisierung mit sich? <hr>
Zentrale Herausforderungen, die sich aus der Digitalisierung ergeben, beziehen sich auf den Widerstand in Bezug auf Veränderungen, Schwierigkeiten in der Durchführung und Koordination der Digitalisierung sowie einen hohen Zeit- und Kostenaufwand, der mit dem digitalen Wandel verbunden ist.
## Grundlagen von Datenschutz <hr>
Zentraler Grundsatz der DSGVO und des gesamten Datenschutzrechts ist das sogenannte Verbotsprinzip. Dies bedeutet, dass die Verarbeitung personenbezogener Daten grundsätzlich verboten und ausnahmsweise nur dann gestattet ist, wenn die Voraussetzungen einer der Erlaubnisnormen der DSGVO greift
### Begriffe erklärt: <hr>
### Datenschutz: 
Datenschutz beschreibt den Schutz vor der missbräuchlichen Verarbeitung personenbezogener Daten sowie den Schutz des Rechts auf informationelle Selbstbestimmung.
## Datensicherheit: 
Datensicherheit ist die praktische Umsetzung des Schutzes digitaler Informationen gegen unbefugten Zugriff, Beschädigung oder Diebstahl während des gesamten Lebenszyklus.
Besonders schützenswerte Daten sind:
### Was gilt als besonders schützenswerte Daten? besonders schützenswerte Personendaten: Daten über: <hr> 
1. die religiösen, weltanschaulichen, politischen oder gewerkschaftlichen Ansichten oder Tätigkeiten, 
2. die Gesundheit, die Intimsphäre oder die Rassenzugehörigkeit, 
3. Masnahmen der sozialen Hilfe.
