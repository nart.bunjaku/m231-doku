## 26.09.23

#### Backup-Konzept

### Cloud-Backups

Ich verwende OneDrive von Microsoft, aber es gibt noch viele andere Cloud-Dienste. Mit Clouds kann man von überall mit seinem Login auf die Dateien zugreifen, welche in der Cloud gespeichert sind. Die meisten Clouds ermöglichen es einem, ältere Versionen wiederherzustellen. Für eine bezahlte Version kann man oft mehr Speicherplatz erhalten, um mehr Cloudspeicher zur Verfügung zu haben. Die Daten sind außerdem auf externen Servern gespeichert, sodass man nicht mitbekommt, wenn jemand darauf zugreift. Falls etwas mit den Servern geschieht, auf denen man Daten gespeichert hat, benötigt man im Notfall eine lokale Sicherung.

### Lokale Backups

Neben Cloud-Backups ist es auch wichtig, lokale Sicherungskopien meiner Dateien zu erstellen.

### Externe Festplatten:

Der Kauf einer externen Festplatte und die regelmäßige Sicherung von Dateien darauf ist eine einfache Möglichkeit, lokale Backups zu erstellen. So kann man Dateien manuell kopieren oder eine Backup-Software verwenden, um den Prozess zu automatisieren.

### Network Attached Storage (NAS):

Ein NAS-Gerät ist eine dedizierte Netzwerkspeicherlösung, die es einem ermöglicht, Dateien in seinem lokalen Netzwerk zu sichern. So kann man NAS-Geräte so konfigurieren, dass sie automatische Backups von Ihren Computern durchführen.

### USB-Sticks:

Kleine USB-Speichergeräte sind praktisch, um wichtige Dateien zu sichern, insbesondere wenn man sie mobil benötigt. So kann man Dateien manuell darauf kopieren oder sie mit Backup-Software sichern.
