# Aufgabe Impressum, Discaimer, AGB
<hr>


+ Aufgabe 1: Übersetzen Sie "Impressum" auf folgende Sprachen

+ Deutsch (ein Synonym oder eine Alternative): Vermerk
- Englisch : imprint
* Französisch : empreinte
+ Italienisch : imprimere
- Spanisch : impresión



+ Aufgabe 2: Was bedeutet ein "Impressum"?

+ Ein "Impressum" ist eine Information, die in Websites und gedruckten Publikationen angegeben werden muss. Sie enthält Angaben zur Identität des Verantwortlichen oder Herausgebers, wie Name, Adresse und Kontaktinformationen. Das Ziel ist, die Transparenz zu fördern und die Nutzer darüber zu informieren, wer hinter der Website oder Publikation steht. Dies dient dem Verbraucherschutz und der Nachvollziehbarkeit.



+ Aufgabe 3: Wann besteht in der Schweiz Impressum-Pflicht? Und wann nicht?

+ In der Schweiz ist es seit dem 1. Januar 2012 Pflicht, auf allen Online-Präsenzen, die geschäftliche Zwecke verfolgen, ein    Impressum bereitzustellen.
+ Private Websites oder rein informative Angebote müssen kein Impressum bereitstellen.



+ Aufgabe 4: Wann besteht in Deutschland (oder in der EU) Impressum-Pflicht? Und wann nicht? Gibt es Unterschiede zur Schweiz?

+ Wer Telemedien geschäftsmäßig oder journalistisch nutzt, benötigt ein Impressum. Das betrifft nicht nur Unternehmen, Behörden, Stiftungen oder Vereine, sondern auch öffentliche Blogs oder werbefinanzierte Webseiten.
+ Private oder familiäre Telemedien sind ausgeschlossen. Das heißt, wenn Sie eine Webseite nur für Familie und enge Freunde betreiben, benötigen Sie kein Impressum. Sobald Werbung geschaltet wird, gilt dies nicht mehr.



+ Aufgabe 5: Übersetzen Sie "Disclaimer" auf folgende Sprachen

+ Deutsch: Verzichtserklärung
- Englisch (ein Synonym oder eine Alternative): clause
* Französisch: démenti
+ Italienisch: disconoscimento
- Spanisch: Renuncia



+ Aufgabe 6: Was bedeutet / was ist ein "Disclaimer" und warum macht man sowas auf eine Internetauftritt (Homepage, Website)?

+ Ein "Disclaimer" ist eine rechtliche Erklärung, die auf einer Website verwendet wird, um Haftung zu begrenzen oder bestimmte Informationen und Warnungen darzulegen.



+ Aufgabe 7: Was bedeuten die drei Buchstaben "AGB" und was steht da so in Etwa drin? Machen Sie in 3-4 ausgeschriebenen Sätzen eine kleine selbstgeschriebene Zusammenfassung.

+ AGB steht für "Allgemeine Geschäftsbedingungen". Das sind schriftliche Regeln, die Unternehmen erstellen, um festzulegen, wie ihre Produkte oder Dienstleistungen genutzt werden dürfen. In den AGB findet man Informationen über Preise, Zahlungen, Garantien, Regeln für die Rückgabe von Produkten und wie mit Problemen umgegangen wird. Bevor man etwas bei einem Unternehmen kauft oder nutzt, sollte man die AGB lesen und verstehen.



+ Aufgabe 8: Zeigen Sie 3 verschiedene Links auf AGB's von Firmen oder auch Vorlagen von AGBs für den eigenen Gebrauch. (bei Abgabe gleicher Links wie Ihre Schulkolleg:innen wird die Aufgabe zur Überarbeitung zurückgegeben)

Amazon: [Amazon AGBs](https://www.amazon.de/gp/help/customer/display.html%3FnodeId%3DGLSBYFE9MGKKQXXM)

Spotify: [Spotify-AGB](https://www.spotify.com/de/legal/end-user-agreement/)

Vorlage: [AGB-Erstellen](https://www.baloise.ch/de/unternehmenskunden/firma-gruenden/nach-der-gruendung/marketing/agb-erstellen.html)
